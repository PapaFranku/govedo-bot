#Discord Bot

A simple bot I made for my friend' Discord channel.

Requires Node.js and TypeScript to build and run.

Run `npm install` to install dependencies.
Run `npm start` to build and start.

You will also need an application token from Discord, which you need to paste in the `index.ts` file.