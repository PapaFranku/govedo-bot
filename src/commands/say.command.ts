import { Command } from "./command";
import { Message } from "discord.js";

export class SayCommand implements Command
{
    commandName: string = "!say";

    getHelpMessage(): string
    {
        return "Example: !say Hello";
    }


  reply(message: Message ,args?: string[]): void
    {
        if(args)
        {
            message.channel.send(args.join(' '), { tts: true });
        }
        else
        {
            message.reply("Tell me what to say, you monkey!");
        }
    }
}