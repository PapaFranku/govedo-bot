import { Command } from "./command";
import { Message, RichEmbed } from "discord.js";

export class FeelsBadCommand implements Command
{
    commandName: string = "!feelsbad";

    getHelpMessage(): string
    {
        return "Feelsbadman meme!";
    }

    reply(message: Message ,args?: string[]): void
    {
        var embed: RichEmbed = new RichEmbed();
        embed.setImage("http://i.imgur.com/aSVjtu7.png");

        message.channel.send("", { "embed": embed } );
    }
}