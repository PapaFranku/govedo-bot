import { Command } from "./command";
import { Message, RichEmbed } from "discord.js";

export class HenryCommand implements Command
{
    commandName: string = "!henry";

    getHelpMessage(): string
    {
        return "It's Henry the Hoover!";
    }

    reply(message: Message ,args?: string[]): void
    {
        var embed: RichEmbed = new RichEmbed();
        embed.setImage("https://cdn.shopify.com/s/files/1/0058/9272/products/00001755_large.jpg?v=1288781474");

        message.channel.send("", { "embed": embed } );
    }
}