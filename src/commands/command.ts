import { Message } from "discord.js";

export interface Command
{
    reply(message: Message ,args?: string[]): void
    getHelpMessage(): string
    commandName: string;
}