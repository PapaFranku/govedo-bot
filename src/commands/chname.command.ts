import { Command } from "./command";
import { Message, GuildChannel } from "discord.js";

export class ChNameCommand implements Command
{
    commandName: string = "!chname";

    getHelpMessage(): string
    {
        return "Sets the name for the current channel.\nExample: !chname general";
    }


    reply(message: Message ,args?: string[]): void
    {
        if(!message.guild.available)
        {
            message.reply("Server is unavailable...");
        }
        else if(args)
        {
            var channels: GuildChannel[] = message.guild.channels.array();

            for(var i = 0; i < channels.length; i++)
            {
                if(channels[i].id == message.channel.id)
                {
                    channels[i].setName(args[0])
                        .catch(err => 
                        {
                            message.reply("You don't have permission for this!");
                        });
                    break;
                }
            }
        }
        else
        {
            message.reply("Tell me a name, you monkey!");
        }
    }
}
