import { Command } from "./command";
import { CommandFactory } from "../core/commandFactory";
import { Message, RichEmbed } from "discord.js";

const commands: string[] = 
[
    "!help", "!help [command]", "!say [sentence]", "!roll", "!cease",
    "!myroles", "!feelsbad", "!henry", "!chname [name]", "!topic [sentence]"
]

export class HelpCommand implements Command
{
    commandName: string = "!help";
    
    constructor(private commandFactory: CommandFactory) { }

    getHelpMessage(): string
    {
        return "Type '!help' for list of commands or " + 
               "'!help [command]' to get help for a command.\n" + 
               "Example: !help !say";
    }

        
    reply(message: Message ,args?: string[]): void
    {
        if(!args)
        {
            var embed: RichEmbed = new RichEmbed();
            embed.setDescription(commands.join(', '));

            message.channel.send("", { "embed": embed } );
        }
        else
        {
            var command: Command = this.commandFactory.createCommand(args[0]);

            message.reply(command.getHelpMessage());
        }
    }
}