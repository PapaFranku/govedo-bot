import { Command } from "./command";
import { Message, GuildMember } from "discord.js";

export class MyRolesCommand implements Command
{
    commandName: string = "!myroles";

    getHelpMessage(): string
    {
        return "Lists your roles.";
    }
    
    reply(message: Message ,args?: string[]): void
    {
        message.guild.fetchMember(message.author.id)
            .then((user: GuildMember) => 
            { 
                var roles: string = user.roles.array().join(' ');
                message.reply(roles);
            })
            .catch(error => { message.reply("Something went wrong..."); });
    }
}