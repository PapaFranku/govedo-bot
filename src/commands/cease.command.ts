import { Command } from "./command";
import { Message, RichEmbed } from "discord.js";

export class CeaseCommand implements Command
{
    commandName: string = "!cease";

    getHelpMessage(): string
    {
        return "Cease meme!";
    }

    reply(message: Message ,args?: string[]): void
    {
        var embed: RichEmbed = new RichEmbed();
        embed.setImage("https://cdn.discordapp.com/attachments/277244121331138560/285264307309117442/b5I9kq5.jpeg");

        message.channel.send("", { "embed": embed } );
    }
}