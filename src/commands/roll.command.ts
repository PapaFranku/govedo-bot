import { Command } from "./command";
import { Message } from "discord.js";

export class RollCommand implements Command
{
    commandName: string = "!roll";

    getHelpMessage(): string
    {
        return "Get a random number between 1 - 100.";
    }
    
    reply(message: Message ,args?: string[]): void
    {
        var randNum = Math.floor(Math.random() * 100) + 1;

        message.channel.send(message.author.username + " rolled " 
            + randNum.toString() + "!");
    }
}