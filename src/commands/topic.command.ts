import { Command } from "./command";
import { Message, GuildChannel, RichEmbed } from "discord.js";

export class TopicCommand implements Command
{
    commandName: string = "!topic";

    getHelpMessage(): string
    {
        return "Sets the topic for the current channel.\nExample: !topic chill";
    }


    reply(message: Message ,args?: string[]): void
    {
        if(!message.guild.available)
        {
            message.reply("Server is unavailable...");
        }
        else if(args)
        {
            var channels: GuildChannel[] = message.guild.channels.array();

            for(var i = 0; i < channels.length; i++)
            {
                if(channels[i].id == message.channel.id)
                {
                    var newTopic = args.join(' ');

                    channels[i].setTopic(newTopic)
                        .then(success => 
                        {
                            var embed: RichEmbed = new RichEmbed();
                            embed.setDescription("The topic has been changed to " 
                                + "\'" + newTopic + "\'");

                            message.channel.send("", { "embed": embed } );
                        })
                        .catch(err => 
                        {
                            message.reply("You don't have permission for this!");
                        });

                    break;
                }
            }
        }
        else
        {
            message.reply("Tell me a name, you monkey!");
        }
    }
}