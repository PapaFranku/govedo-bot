import { Command } from "./command";
import { Message } from "discord.js";

export class UnrecognisedCommand implements Command
{
    commandName: string = "";

    getHelpMessage(): string
    {
        return "No such command exists!";
    }

    reply(message: Message ,args?: string[]): void
    {
        message.reply("This command was not recognised. Type '!help' for list of commands!");
    }
}