import { Client, Message, GuildMember, Guild } from "discord.js";
import { MessageHandler } from "./core/messageHandler";

const govedo = new Client();
const messageHandler: MessageHandler = new MessageHandler();

govedo.on("message", (message: Message) =>
{
    messageHandler.handle(message);
});

govedo.on("guildMemberAdd", (member: GuildMember) =>
{
    member.guild.defaultChannel.send("Hello " + member.displayName +
        ", welcome to the server!");
});

govedo.login("");
