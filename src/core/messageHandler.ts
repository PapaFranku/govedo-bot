import { Message } from "discord.js";
import { CommandFactory } from "./commandFactory";
import { Command } from "../commands/command";

export class MessageHandler
{
    private command: Command;
    private commandFactory: CommandFactory

    constructor()
    {
        this.commandFactory = new CommandFactory();
    }

    public handle(message: Message): void
    {
        if(!message.content.startsWith('!') || message.author.bot)
            return;

        var toArray: string[] = message.content.split(' ');
        toArray[0] = toArray[0].toLowerCase();

        this.command = this.commandFactory.createCommand(toArray[0]);

        if(toArray.length == 1)
            this.command.reply(message);
        else
        {
            var args: string[] = toArray.slice(1, toArray.length);
            this.command.reply(message, args);
        }          
    }
}