import { Command } from "../commands/command";
import { Message } from "discord.js";
import * as cmd from "../commands/index";

export class CommandFactory
{
    public createCommand(commandType: string): Command
    {
        switch(commandType)
        {
            case "!say":
                return new cmd.SayCommand();
            case "!cease":
                return new cmd.CeaseCommand();
            case "!roll":
                return new cmd.RollCommand();
            case "!help":
                return new cmd.HelpCommand(this);
            case "!myroles":
                return new cmd.MyRolesCommand();
            case "!feelsbad":
                return new cmd.FeelsBadCommand();
            case "!henry":
                return new cmd.HenryCommand();
            case "!chname":
                return new cmd.ChNameCommand();
            case "!topic":
                return new cmd.TopicCommand();
            default:
                return new cmd.UnrecognisedCommand();
        }
    }
}